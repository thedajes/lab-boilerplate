/* eslint-disable no-console */
import * as mock from './lab3/mock_for_L3';
import * as task1 from './lab3/task1';
import * as task2 from './lab3/task2';
import filterProfiles from './lab3/task3';
import sortByProperty from './lab3/task4';
import filterByProperty from './lab3/task5';
import percentageOfFilteredProperties from './lab3/task6';
import { validate } from './lab3/task2';

import('../css/style.scss');

const array = mock.randomUserMock;
const { additionalUsers } = mock;
const output = [];
let repaintTeachers = () => {
};
let repaintStatisticTable = () => {
};

console.log('\n\n\nTask 1');
task1.flatten(array, output);
console.log('random_user_mock:', output);
console.log('additional_users:', additionalUsers);
console.log('merged:', task1.merge(output, additionalUsers));

console.log('\n\n\nTask 2');
task2.validate(output[23]);

console.log('\n\n\nTask 3');
console.log(filterProfiles(output, 'gender', 'female'));

console.log('\n\n\nTask 4');
sortByProperty(output, 'full_name', false);
console.log(output);

console.log('\n\n\nTask 5');
console.log(filterByProperty(output, 'age', 69));

console.log('\n\n\nTask 6');
console.log(`${Math.round(percentageOfFilteredProperties(output, 'age', 38))}%`);

const teacherFilterBox = document.getElementById('teachersFilter');
const statisticTable = document.getElementsByClassName('statisticTable__table')[0];

const toggleFavorite = (user, callbackAfterRepaint) => {
  // eslint-disable-next-line no-param-reassign
  user.favorite = !user.favorite;
  repaintTeachers(output);
  callbackAfterRepaint();
};

const showTeacherInfo = (user) => {
  const verboseTeachCardBox = document.getElementById('verboseTeacherCard');
  window.location.hash = '#verboseTeacherCard';
  const starElement = user.favorite ? '<img class="teacherCard__starIcon" src="images/star.jpg" alt="yellow star">\n' : '';
  verboseTeachCardBox.innerHTML = `
   <div class="verboseTeacherCard">
        <section class="verboseTeacherCard__section teacherCard">
            <a href="#" title="Close" class="modal-close"><img src="images/cross.png" alt="close"></a>
            ${starElement}
            <img class="teacherCard__img" src="${user.picture_large}" alt="teacher picture">
            <h2 id="card_fullName1" aria-label="full name"> ${user.full_name}</h2>
            <h3 id="card_location1"> ${user.city}, ${user.country}</h3>
            <h3 id="card_ageGender1">${user.age}, ${user.gender}</h3>
            <a href="mailto:${user.email}"><h3 id="card_email1">${user.email}</h3></a>
            <h3 id="card_phone1">${user.phone}</h3>
            <div class="clear">
                <p id=card_commentary1">${user.note} Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin finibus erat non
                    mollis
                    consectetur. Vivamus aliquam est libero. Duis fringilla arcu ac lacus tincidunt, at semper ipsum
                    euismod. </p>
                <a class="teacherCard__mapToggle" href="">toggle map</a>
            </div>
            <button>Toggle favorite</button>
        </section>
    </div>
  `;
  const htmlButtonElement = verboseTeachCardBox.querySelector('button');
  htmlButtonElement.addEventListener('click', toggleFavorite.bind(null, user, showTeacherInfo.bind(null, user)));
};

const renderNewTeacherElement = (user) => {
  const newMovieElement = document.createElement('div');
  newMovieElement.className = 'teacherThumbnail';
  const starElement = user.favorite ? '<img class="teacherThumbnail__starIcon" src="images/star.jpg" alt="yellow star">' : '';
  newMovieElement.innerHTML = `

   <div class="teacherThumbnail">
                <a href="#verboseTeacherCard">
                    <img class="teacherThumbnail__img" src="${user.picture_large}" alt="Picture1">
                </a>
                ${starElement}
                <p class="teacherThumbnail__info">
                    <span class="teacherThumbnail__name">
                        ${user.full_name}
                    </span>
                    <span class="teacherThumbnail__country">
                        ${user.country}
                    </span>
                </p>
            </div>
  `;
  const teacherBoxRoot = document.getElementById('teacherBox');
  newMovieElement.querySelector('img').addEventListener('click', showTeacherInfo.bind(null, user));
  teacherBoxRoot.append(newMovieElement);
};

const renderStatisticTableElement = (user) => {
  const rowElement = document.createElement('tr');
  rowElement.innerHTML = `
  <td>${user.full_name}</td>
                <td>${user.age}</td>
                <td>${user.b_date}</td>
                <td>${user.nationality}</td>
  `;
  const tableBody = statisticTable.getElementsByTagName('tbody')[0];
  tableBody.append(rowElement);
};

const clearTeacherBox = () => {
  const teacherBoxRoot = document.getElementById('teacherBox');
  while (teacherBoxRoot.children.length !== 0) {
    teacherBoxRoot.children[0].remove();
  }
};

const clearStatisticTable = () => {
  const tableBody = statisticTable.getElementsByTagName('tbody')[0];
  while (tableBody.children.length !== 0) {
    tableBody.children[0].remove();
  }
};

repaintTeachers = (teachers) => {
  clearTeacherBox();
  for (let i = 0; i < 10; i += 1) {
    renderNewTeacherElement(teachers[i]);
  }
};

repaintStatisticTable = (teachers) => {
  clearStatisticTable();
  for (let i = 0; i < 10; i += 1) {
    renderStatisticTableElement(teachers[i]);
  }
};

const tableSortHandler = (event) => {
  let sorted;
  switch (event.target.innerHTML) {
    case 'Full name':
      sorted = sortByProperty([...output], 'full_name');
      break;
    case 'Age':
      sorted = sortByProperty([...output], 'age');
      break;
    case 'Birthdate':
      sorted = sortByProperty([...output], 'b_date');
      break;
    case 'Nationality':
      sorted = sortByProperty([...output], 'nationality');
      break;
    default:
      sorted = [];
  }
  repaintStatisticTable(sorted);
};

const radioFilterHandler = (event) => {
  let filtered;
  switch (event.target.value) {
    case '':
      filtered = output;
      break;
    case 'France':
      filtered = filterProfiles(output, 'country', 'France', '=');
      break;
    case 'favorite':
      filtered = filterProfiles(output, 'favorite', true, '=');
      break;
    case 'age':
      filtered = filterProfiles(output, 'age', 30, '>');
      break;
    case 'male':
      filtered = filterProfiles(output, 'gender', 'male', '=');
      break;
    default:
      filtered = [];
  }
  repaintTeachers(filtered);
};

teacherFilterBox.querySelectorAll('input').forEach((elem) => {
  elem.addEventListener('change', radioFilterHandler);
});

const addTableListeners = () => {
  const { children } = statisticTable.getElementsByTagName('tr')[0];
  // eslint-disable-next-line no-restricted-syntax
  for (const header of children) {
    header.addEventListener('click', tableSortHandler);
  }
};

const searchButtonHandler = (event) => {
  const searchInputValue = event.target.parentNode.getElementsByTagName('input')[0].value;
  console.log(searchInputValue);
  const user = filterByProperty(output, 'full_name', searchInputValue);
  console.log('user: ', user);
  if (user !== undefined) showTeacherInfo(user);
};

const addSearchButtonListeners = () => {
  const searchButton = document.getElementsByClassName('searchButton');
  // eslint-disable-next-line no-restricted-syntax
  for (const searchButtonElement of searchButton) {
    searchButtonElement.addEventListener('click', searchButtonHandler);
  }
};

const addTeacherHandler = (event) => {
  event.preventDefault();
  const userInputs = event.target.querySelectorAll('input');
  const selectInput = event.target.querySelector('select');
  const newTeacher = {};
  newTeacher[selectInput.name] = selectInput.value;
  userInputs.forEach((input) => {
    if (input.checked === false && input.type === 'radio') {
      return;
    }
    newTeacher[input.name] = input.value;
  });
  console.log('newTeacher: ', newTeacher);
  const errors = validate(newTeacher);
  if (errors.length !== 0) {
    console.log('errors: ', errors);
    errors.forEach((error) => {
      // eslint-disable-next-line no-param-reassign
      event.target.innerHTML += `<p>\nError in ${error.field}: ${error.error}</p>`;
    });
    return;
  }
  window.location.hash = '#';
  output.push(newTeacher);
};

const addListenersForAddTeacher = () => {
  const addTeacherForm = document.querySelectorAll('form')[0];
  addTeacherForm.addEventListener('submit', addTeacherHandler);
};

repaintTeachers(output);
repaintStatisticTable(output);
addTableListeners();
addSearchButtonListeners();
addListenersForAddTeacher();
