export default function percentageOfFilteredProperties(arr, property, value) {
  const filterParams = 'full_name,note,age'.split(',');
  if (!filterParams.includes(property)) {
    return 0;
  }
  const filtered = arr.filter((item) => item[property] === value);
  return (filtered.length / arr.length) * 100;
}
