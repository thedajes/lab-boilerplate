export function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    // eslint-disable-next-line no-bitwise
    const r = Math.random() * 16 | 0;
    // eslint-disable-next-line no-bitwise,no-mixed-operators
    const v = c === 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

export function flatten(array, output) {
  array.forEach((_user) => {
    const user = {};
    user.coordinates = {};
    user.timezone = {};
    user.gender = _user.gender;
    user.title = _user.name.title;
    user.full_name = `${_user.name.first} ${_user.name.last}`;
    user.city = _user.location.city;
    user.state = _user.location.state;
    user.country = _user.location.country;
    user.postcode = _user.location.postcode;
    user.coordinates = Object.assign(user.coordinates, _user.location.coordinates);
    user.timezone = Object.assign(user.timezone, _user.location.timezone);
    user.email = _user.email;
    user.b_date = _user.dob.date;
    user.age = _user.dob.age;
    user.phone = _user.phone;
    user.picture_large = _user.picture.large;
    user.picture_thumbnail = _user.picture.thumbnail;
    user.id = uuidv4();
    user.favorite = false;
    user.course = 'math';
    user.bg_color = '#1f75cb';
    user.note = 'Some note';
    user.nationality = _user.nat;
    output.push(user);
  });
}

export function merge(baseArray, additionalArr) {
  additionalArr.forEach((el) => {
    if (!baseArray.find((obj) => el.full_name === obj.full_name)) baseArray.push(el);
  });
  return baseArray;
}
