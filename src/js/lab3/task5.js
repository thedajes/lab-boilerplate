export default function filterByProperty(arr, property, value) {
  const filterParams = 'full_name,note,age'.split(',');
  if (!filterParams.includes(property)) {
    return [];
  }
  return arr.filter((item) => item[property] === value)[0];
}
