import phone from 'phone';
import countries from './countries.json';

export function getCountryCode(countryName) {
  const country = countries.find((element) => element.name === countryName);
  if (country === undefined) return 'UA';
  return country['alpha-3'];
}

export function startsWithUpper(str) {
  return str[0] === str[0].toUpperCase();
}

export function validate(obj) {
  const errors = [];
  const fields = 'full_name, gender, note, state, city, country'.split(', ');
  // eslint-disable-next-line no-console
  fields.forEach((element) => {
    if (obj[element] !== undefined) {
      if (!startsWithUpper(obj[element])) {
        errors.push({ field: element, error: 'no uppercase' });
      }
    }
  });

  if (Number.isNaN(obj.age)) {
    errors.push({ field: 'age', error: 'is NaN' });
  }
  if (phone(obj.phone, getCountryCode(obj.country), true).length !== 2) {
    errors.push({ field: 'phone', error: 'is invalid' });
  }
  return errors;
}
