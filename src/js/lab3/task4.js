export default function sortByProperty(arr, property, ascending = true) {
  const sortParams = 'full_name,age,b_day,country'.split(',');
  if (!sortParams.includes(property)) {
    return arr;
  }
  if (ascending) {
    arr.sort((a, b) => {
      if (a[property] > b[property]) return 1;
      if (a[property] < b[property]) return -1;
      return 0;
    });
  } else {
    arr.sort((a, b) => {
      if (a[property] < b[property]) return 1;
      if (a[property] > b[property]) return -1;
      return 0;
    });
  }
  return arr;
}
