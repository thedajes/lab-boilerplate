export default function filterProfiles(arr, filterParam, paramValue, operator) {
  const allowedParams = 'country, age, gender, favorite'.split(', ');
  if (!allowedParams.includes(filterParam)) {
    return [];
  }
  switch (operator) {
    case '=': return arr.filter((item) => item[filterParam] === paramValue);
    case '>': return arr.filter((item) => item[filterParam] > paramValue);
    case '<': return arr.filter((item) => item[filterParam] > paramValue);
    case 'in': return arr.filter((item) => paramValue.includes(item[filterParam]));
    default: return [];
  }
}
